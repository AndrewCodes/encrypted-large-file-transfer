#include "helper.h"

// const char* EncryptSesh(string message, BLOWFISHSESSION blowfish){
// 	string encrypted = blowfish.Encrypt_CBC(message);
// 	return encrypted.c_str();
// }
//
// const char* EncryptAlice(string message, BLOWFISHALICE blowfish){
// 	string encrypted = blowfish.Encrypt_CBC(message);
// 	return encrypted.c_str();
// }
//
// const char* EncryptBob(string message, BLOWFISHBOB blowfish){
// 	string encrypted = blowfish.Encrypt_CBC(message);
// 	return encrypted.c_str();
// }


long f(long nonce) {
    const long A = 48271;
    const long M = 2147483647;
    const long Q = M/A;
    const long R = M%A;

	static long state = 1;
	long t = A * (state % Q) - R * (state / Q);

	if (t > 0)
		state = t;
	else
		state = t + M;
	return (long)(((double) state/M)* nonce);
}

int initSocket(const char* IP, short PORT){
	int socketInt;
	struct sockaddr_in serv_addr;
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);
	if((socketInt = socket(AF_INET, SOCK_STREAM, 0)) < 0){
		cout << "Socket creation error" << endl;
		return -1;
	}

	if(inet_pton(AF_INET, IP, &serv_addr.sin_addr) <= 0){
		cout << "Invalid *address/ *address not supported" << endl;
		return -1;
	}
	if(connect(socketInt, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0){
		cout << "Connection Failed" << endl;
		return -1;
	}

	return socketInt;
}

int initServer(short PORT, int* server_fd, struct sockaddr_in* address){
	int new_socket, valread;
	int opt = 1;
	int addrlen = sizeof(*address);
	// Creating socket file descriptor
	if ((*server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0){
		perror("socket failed");
		exit(EXIT_FAILURE);
	}
		// Forcefully attaching socket to the port 8080
	if (setsockopt(*server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))){
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}
	address->sin_family = AF_INET;
	address->sin_addr.s_addr = INADDR_ANY;
	address->sin_port = htons( PORT );
		// Forcefully attaching socket to the port 8080
	if (bind(*server_fd, (struct sockaddr*)address, sizeof(*address))<0) {
		perror("bind failed");
		exit(EXIT_FAILURE);
	}
	if (listen(*server_fd, 3) < 0){
		perror("listen");
		exit(EXIT_FAILURE);
	}
	if ((new_socket = accept(*server_fd, (struct sockaddr*)address, (socklen_t*)&addrlen))<0) {
		perror("accept");
		exit(EXIT_FAILURE);
	}

	return new_socket;
}
