all: Alice Bob KDC

helper.o: helper.h helper.cc
	g++ helper.cc -Wall -c -std=c++11

Bob: Bob.o helper.o
	g++ helper.o Bob.o -o Bob
Bob.o: Bob.cc
	g++ Bob.cc -Wall -c -std=c++11

KDC: KDC.o helper.o
	g++ helper.o KDC.o -o KDC
KDC.o: KDC.cc
	g++ KDC.cc -Wall -c -std=c++11

Alice: helper.o Alice.o
	g++ helper.o Alice.o -o Alice
Alice.o: Alice.cc
	g++ Alice.cc -Wall -c -std=c++11

clean:
	rm *.o
