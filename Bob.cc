#include <bits/stdc++.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <iostream>
#include "blowfishBob.h"
#include "blowfishSession.h"
#include "helper.h"
using namespace std;
#define PORT 9437
#define BUFFSIZE 1024

string key_bob = "";
string destination = "";
unsigned long nonce2 = 0;

int userInput = 1;

int main(int argc, char const *argv[]){

	cout << "Input Bob's Key: ";
	cin >> key_bob;
	cout << endl;

	cout << "Input Bob's Nonce: ";
	string input;
	cin >> input;
	nonce2 = stol(input);
	cout << endl;

    cout << "Input output file name/path: ";
	cin >> destination;
	cout << endl;


    cout << "Bob Key: " << key_bob << endl;

	int* server_fd = (int*)malloc(sizeof(int));
	struct sockaddr_in* address = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));
	int aliceSocket = initServer(PORT, server_fd, address);

	BLOWFISHBOB blowBob(key_bob);

	cout << "Receiving message from client......." << endl << endl;

	char buffer[BUFFSIZE] = {0};
	read(aliceSocket , buffer, BUFFSIZE);
	string stri(buffer);
	cout << "Received From Alice: "<< stri << endl;

	string decryptedFromAlice = blowBob.Decrypt_CBC(stri);
	cout << "Decrypted from alice: " << decryptedFromAlice << endl;

	string key_sesh = decryptedFromAlice.substr(0,decryptedFromAlice.find(","));

    cout << "Session key: " << key_sesh << endl;

	BLOWFISHSESSION blowSesh(key_sesh);

	// SENDING ENCRYPTED NONCE2 TO ALICE
	string nonce2Str = to_string(nonce2);
	nonce2Str = blowSesh.Encrypt_CBC(nonce2Str);

	const char* nonce2StrPtr = nonce2Str.c_str();
	send(aliceSocket, nonce2StrPtr, strlen(nonce2StrPtr), 0);


	// READING FUNCTION FROM ALICE
	memset(buffer, '\0', BUFFSIZE);
	read(aliceSocket, buffer, BUFFSIZE);
	cout <<"Encrypted function: " << buffer << endl;

	string encryptedFunction(buffer);
	string unencryptedFunction = blowSesh.Decrypt_CBC(encryptedFunction);
	cout << "Decrypted function: " << unencryptedFunction << endl;

    cout << "Calculated function (Should match above) : " << f(nonce2) << endl;

    cout << "**********************************" << endl;

    // Part 1.5 - Test String
    memset(buffer, '\0', BUFFSIZE);
	read(aliceSocket, buffer, BUFFSIZE);
    string testString(buffer);

	cout << "Encrypted test message: " << testString << endl;

    testString = blowSesh.Decrypt_CBC(testString);
    cout << "Decrypted test message: " << testString << endl;

    stringstream ss;
    for(int i=0; i<testString.length(); i++){
        ss << hex << (int)testString[i];
    }
    cout << "In hex: " << ss.str() << endl;

    cout << "**********************************" << endl;


    // Part 2
    long long numChunks = 0;
    recv(aliceSocket , &numChunks, sizeof(numChunks), 0); // Receive Num Chunks
    cout << "Num Chunks: " << numChunks << endl;

    ofstream out(destination, ios::out);

    cout << "Receiving and decrypting..." << endl;
    for (int i = 0; i < numChunks ; i++) {

		//Rec size
		int sizeOfEncrString = 0;
		recv(aliceSocket , &sizeOfEncrString, sizeof(sizeOfEncrString), 0);

		// Rec string
		string chunkyBufferAsStr = "";
		int tempSize = sizeOfEncrString;
		while(tempSize > 0){
			char chunkyBuffer[tempSize + 5];
			memset(chunkyBuffer, '\0', (sizeof(chunkyBuffer)));
			tempSize -= recv(aliceSocket, &chunkyBuffer, (sizeof(chunkyBuffer) - 5), 0);
			chunkyBufferAsStr += string(chunkyBuffer);
		}

		chunkyBufferAsStr = chunkyBufferAsStr.substr(0,sizeOfEncrString);
		chunkyBufferAsStr = blowSesh.Decrypt_CBC(chunkyBufferAsStr);

        out << chunkyBufferAsStr; // Write to file
    }
    cout << "Done!" << endl;

    out.close();

	close(*server_fd);
	free(server_fd);
	return 0;
}
