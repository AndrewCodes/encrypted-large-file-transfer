#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>

using namespace std;

// const char* EncryptSesh(string message, BLOWFISHSESSION blowfish);
//
// const char* EncryptAlice(string message, BLOWFISHALICE blowfish);
//
// const char* EncryptBob(string message, BLOWFISHBOB blowfish);

int initSocket(const char* IP, short PORT);
int initServer(short PORT, int* server_fd, struct sockaddr_in* address);
long f(long nonce);
