AUTHORS: Andrew Collins, Nolan Forehand, Jake Oliversen, Eric Sell

==== COMPILE ====
> make 

==== RUN ====
Run the following in order. Enter all required inputs before proceeding to the next

THING1: >./kdc
THING3: >./bob
THING0: >./alice

==== ENTERED KEY RESTRICTIONS ====
Keys must be at least 4 hex characters with an even length
	AEDCBA9876543210 is a valid key
	XEDCBA9876543210 is not a valid key
	AEDCBA987654321  is not a valid key

We frequently used
	A12345 - Alice's Key
	B12345 - Bob's Key
	C12345 - Session Key