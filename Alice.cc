#include <bits/stdc++.h>
#include <unistd.h>
#include <fstream>
#include <stdlib.h>
#include "helper.h"
#include "blowfishAlice.h"
#include "blowfishBob.h"
#include "blowfishSession.h"

#define PORT 9428
#define BORT 9437
#define BUFFSIZE 16000

using namespace std;

const char* thing2 = "10.35.195.22"; // if bob is on thing2
const char* thing3 = "10.35.195.49"; // if bob is on thing3
const char* thing1 = "10.35.195.47";
const char* thing0 = "10.35.195.46";
string key_alice = "";
string bobServer = "";
string kdcServer = "";
unsigned long int nonce = 0;

int main(int argc, char const *argv[]){

	const char* bobIP;
	int bobThingNum;
	const char* kdcIP;
	int kdcThingNum;
	string path = "";

	cout << "Input Bob's Server: thing";
	cin >> bobThingNum;
	cout << endl;
	if (bobThingNum == 0) {
		bobIP = thing0;
	} else if (bobThingNum == 1) {
		bobIP = thing1;
	}else if (bobThingNum == 2) {
		bobIP = thing2;
	}else if (bobThingNum == 3) {
		bobIP = thing3;
	}

	cout << "Input KDC's Server: thing";
	cin >> kdcThingNum;
	cout << endl;
	if (kdcThingNum == 0) {
		kdcIP = thing0;
	} else if (kdcThingNum == 1) {
		kdcIP = thing1;
	}else if (kdcThingNum == 2) {
		kdcIP = thing2;
	}else if (kdcThingNum == 3) {
		kdcIP = thing3;
	}

	cout << "Input Alice's Key: ";
	cin >> key_alice;
	cout << endl;

	cout << "Input Alice's Nonce: ";
	string input;
	cin >> input;
	nonce = stol(input);
	cout << endl;

	cout << "Input file name/path: ";
	cin >> path;
	cout << endl;

	BLOWFISHALICE blowAlice(key_alice);

	int kdcSock = initSocket(kdcIP, PORT);	 //Connect to KDC
    int bock = initSocket(bobIP, BORT);		// Connect to Bob

    char buffer[BUFFSIZE] = {0};				// Buffer for reading from socket

	// SEND TO KDC
	string requestToKdc = "";
	string delim = ",";
	requestToKdc = bobIP + delim + to_string(nonce);
	const char* requestToKdcPtr = requestToKdc.c_str();
	send(kdcSock, requestToKdcPtr, strlen(requestToKdcPtr), 0);

    //READ NEW MESSAGE
    read(kdcSock, buffer, BUFFSIZE);
    cout << "Received: " << buffer << endl;
    string requestFromAlice = string(buffer);
    string decrypted = blowAlice.Decrypt_CBC(requestFromAlice);
    cout << "Decrypted: " << decrypted << endl;

	string key_sesh = decrypted.substr(0,decrypted.find(","));

    cout << "Session key: " << key_sesh << endl;
	BLOWFISHSESSION blowSesh(key_sesh);

	string messageForBob = decrypted.substr(decrypted.find("|")+1);
	cout << "Messageforbob: " << messageForBob << endl;
	const char* msgForBob = messageForBob.c_str();
	send(bock, msgForBob, strlen(msgForBob), 0);

	// READING ENCRUPTED NONCE2 FROM BOB
	memset(buffer, '\0', BUFFSIZE);
	read(bock, buffer, BUFFSIZE);
	cout << "Encrypted Nonce2: " << buffer << endl;
	string encrytedNonce2(buffer);
	string unencryptedNonce2 = blowSesh.Decrypt_CBC(encrytedNonce2);
	unsigned long int readNonce2 = stoi(unencryptedNonce2);
	cout << "Nonce2 " << readNonce2 << endl;

	// SENDING NONCE FUNCTION TO BOB
	long functionedNonce = f(readNonce2);
	cout << "The long function: " << functionedNonce << endl;
	string unencryptedFunction = to_string(functionedNonce);
	string encryptedFunction = blowSesh.Encrypt_CBC(unencryptedFunction);
	const char* functionPtr = encryptedFunction.c_str();
	send(bock, functionPtr, strlen(functionPtr), 0);
    cout << "**********************************" << endl;

    // Part 1.5 - Test String
    std::string testString;
    std::cout << "Enter test string: " << endl;
    cin.ignore(); // Clears cin? Otherwise it skips the input
    std::getline (std::cin,testString);

    stringstream ss;
    cout << "testString: " << testString << endl;
    for(int i=0; i<testString.length(); i++){
        ss << hex << (int)testString[i];
    }
    cout << "In hex: " << ss.str() << endl;

    testString = blowSesh.Encrypt_CBC(testString);
    cout << "Encrypted: " << testString << endl;

    const char* testStringPtr = testString.c_str();
	send(bock, testStringPtr, strlen(testStringPtr), 0);

    cout << "**********************************" << endl;
	// PART 2 - SEND TO Bob
    long long numBytesInFile = 0;
    char c;
    fstream sizeReader(path, fstream::in);
    while(sizeReader >> noskipws >> c){
        numBytesInFile++;
    }
	cout << "numBytesInFile: " << numBytesInFile << endl;
	int chunkSize = 16383;
	long long numChunks = numBytesInFile/(chunkSize);

	if (numBytesInFile % chunkSize != 0){
		numChunks++;
	}
	cout << "Num Chunks: " << numChunks << endl;
	send(bock, &numChunks, sizeof(numChunks), 0);

	// Send File
	char ch;
	string tempStr = "";
	fstream fileReader(path, fstream::in);
	while(fileReader >> noskipws >> ch){

        tempStr += ch;

		if (tempStr.length() == chunkSize) {

			tempStr = blowSesh.Encrypt_CBC(tempStr);					// Encrypt

			int sizeOfEncrString = tempStr.length();
			send(bock, &sizeOfEncrString, sizeof(sizeOfEncrString), 0);	// Send size

			const char * tempStrPtr = tempStr.c_str();
			int sentPacket = send(bock, tempStrPtr, strlen(tempStrPtr), NULL);	// Send encrypted text
			tempStr = "";

		}
    }

	// Send last partial chunk
	tempStr = blowSesh.Encrypt_CBC(tempStr);
	int sizeOfEncrString = tempStr.length();
	send(bock, &sizeOfEncrString, sizeof(sizeOfEncrString), 0);
	const char * tempStrPtr = tempStr.c_str();
	int sentPacket = send(bock, tempStrPtr, strlen(tempStrPtr), NULL);

	return 0;
}
