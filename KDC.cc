#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <bitset>
#include <iostream>
#include "blowfishAlice.h"
#include "blowfishBob.h"
#include "blowfishSession.h"
#include "helper.h"
using namespace std;
#define PORT 9428
#define BUFFSIZE 1024

string key_alice = "";
string key_sesh = "";
string key_bob = "";


int userInput = 1;

int main(int argc, char const *argv[]){
	if(userInput){
		cout << "Input Alice's Key: ";
		cin >> key_alice;
		cout << endl;
		cout << "Input Bob's Key: ";
		cin >> key_bob;
		cout << endl;
		cout << "Input Session's Key: ";
		cin >> key_sesh;
		cout << endl;
	}

	BLOWFISHALICE blowAlice(key_alice);
	BLOWFISHBOB blowBob(key_bob);
	BLOWFISHSESSION blowSesh(key_sesh);

	cout << "Alice Key: " << key_alice << endl;
	cout << "Bob Key: " << key_bob << endl;
	cout << "Session Key: " << key_sesh << endl;
	int* server_fd = (int*)malloc(sizeof(int));
	struct sockaddr_in* address = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));
	int aliceSocket = initServer(PORT, server_fd, address);

    //READ ALICE
	char buffer[BUFFSIZE] = {0};
	read(aliceSocket , buffer, BUFFSIZE);
	cout << "First message received: " << buffer << endl;

	// Create Bob's Part
	string delim = ",";
	string delim2 = "|";

    string requestFromAlice(buffer);
	string idA = to_string(address->sin_addr.s_addr);
	string bobsPart = key_sesh + delim + idA;

	bobsPart = blowBob.Encrypt_CBC(bobsPart);
	string toBeEncryptedByKa = key_sesh + delim +  buffer + delim2 + bobsPart;

	string encryptedResponseToAlice = blowAlice.Encrypt_CBC(toBeEncryptedByKa);
	const char* alicePtr = encryptedResponseToAlice.c_str();
	cout << "Sending: " << alicePtr << endl;
    send(aliceSocket, alicePtr, strlen(alicePtr), 0);

    close(*server_fd);
	free(server_fd);
	return 0;
}
